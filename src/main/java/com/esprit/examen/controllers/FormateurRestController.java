package com.esprit.examen.controllers;

import com.esprit.examen.entities.Poste;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.examen.entities.Cours;
import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.TypeCours;
import com.esprit.examen.services.IFormateurService;

import java.util.List;

@RestController
public class FormateurRestController {

	private static final Logger l = LogManager.getLogger(FormateurRestController.class);



	@Autowired
	IFormateurService formateurService;
	
	@PostMapping("/ajouterFormateur")
	@ResponseBody
	public Formateur ajouterFormateur(@RequestBody Formateur formateur) {
		formateurService.addFormateur(formateur);
		l.trace("ajout formateur id = " );
		return formateur;
	}

	@PutMapping("/modifierFormateur")
	@ResponseBody
	public Formateur modifierFormateur(@RequestBody Formateur formateur) {
		formateurService.addFormateur(formateur);
		return formateur;
	}

	@DeleteMapping("/supprimerFormateur/{formateurId}")
	@ResponseBody
	public void supprimerFormateur(@PathVariable("formateurId") Long formateurId) {
		try {
			formateurService.supprimerFormateur(formateurId);
			l.info("suppression avec succes");
		}catch (Exception e)
		{
			l.error("id formateur n existe pas : "+ formateurId);
		}
	}
	
	@GetMapping("/nombreFormateursImpliquesDansUnCours/{typeCours}")
	@ResponseBody
	public int nombreFormateursImpliquesDansUnCours(@PathVariable("typeCours") String typeCours) {
		int nombreFormateurs=formateurService.nombreFormateursImpliquesDansUnCours(typeCours);
		l.info("nombre formateur : "+nombreFormateurs);
		return nombreFormateurs;
	}

	@GetMapping("/listeFormateur")
	@ResponseBody
	public List<Formateur> listeFormateurs() {

		l.info("liste formateur : "+formateurService.listFormateurs());
		return  formateurService.listFormateurs();
	}

	@GetMapping("/listeFormateurParPoste/{poste}")
	@ResponseBody
	public List<Formateur> liste(@PathVariable("poste") Poste poste) {
		return formateurService.listFormateursParPoste(poste);
	}

	@GetMapping("/nombreCoursParFormateur/{formateurId}")
	@ResponseBody
	public int nombreFormateursImpliquesDansUnCours(@PathVariable("formateurId") Long formateurId) {
		int nombreCours=formateurService.nombreCoursParFormateur(formateurId);
		return nombreCours;
	}

}
