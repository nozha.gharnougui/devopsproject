package com.esprit.examen.repositories;

import com.esprit.examen.entities.Poste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.TypeCours;

import java.util.List;


@Repository
public interface FormateurRepository extends JpaRepository<Formateur, Long>{



    @Query(value = "SELECT COUNT(*) FROM formateur " +
			"INNER JOIN session ON formateur_id = formateur.id  " +
			"INNER JOIN session_cours ON session_cours.sessions_id = session.id   " +
			"INNER JOIN cours ON session_cours.cours_id = cours.id   " +
			"where type_cours=:typeCours   "  , nativeQuery = true)


	public int nombreFormateursImpliquesDansUnCours(@Param("typeCours")String typeCours);




	@Query(value = "SELECT COUNT(*) FROM formateur " +
			"INNER JOIN session ON formateur_id = formateur.id  " +
			"INNER JOIN session_cours ON session_cours.sessions_id = session.id   " +
			"INNER JOIN cours ON session_cours.cours_id = cours.id   " +
			"where formateur.id=:id   "  , nativeQuery = true)
	public int nombreCoursParFormateur(@Param("id")Long id);



	public List<Formateur> findByPoste(Poste poste);




}
