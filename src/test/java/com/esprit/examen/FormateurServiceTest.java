package com.esprit.examen;

import com.esprit.examen.entities.*;
import com.esprit.examen.services.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FormateurServiceTest {

    private static final Logger l = LogManager.getLogger(FormateurServiceTest.class);

    @Autowired
    IFormateurService agent ;

    @Autowired
    ISessionService ss ;

    @Autowired
    ICoursService cs ;



    @Test
    public void testaddFormateur(){
        Formateur f = new Formateur("chiraz","demni", Poste.Ingénieur, Contrat.EXPERT,"chiraz@gmail.com","ssssssss");

        Long  id = agent.addFormateur(f);
        l.info("test  ajout  formateur  id : "+id);


        Assert.assertFalse(id == 0);

        Formateur newF = agent.get(id).get();

        Assert.assertTrue(newF.getPoste().equals(Poste.Ingénieur));
        Assert.assertTrue(newF.getContrat().equals(Contrat.EXPERT));



        agent.supprimerFormateur(id);
    }

    @Test
    public void testmodifierFormateur() {
        Formateur f = new Formateur("chiraz","demni", Poste.Ingénieur, Contrat.EXPERT,"chiraz@gmail.com","ssssssss");
        Long  id = agent.addFormateur(f);

        f.setId(id);
        f.setNom("imen");
        f.setPassword("new password");

        Long newId = agent.modifierFormateur(f);

        Assert.assertFalse(newId == 0);
        Assert.assertEquals(id,newId);

        Formateur newF = agent.get(newId).get();
        l.info("test get formateur  pour id = "+ newId +" => "+newF);



        Assert.assertTrue(newF.getNom().equals("imen"));
        Assert.assertTrue(newF.getPassword().equals("new password"));
        Assert.assertTrue(newF.getContrat().equals(Contrat.EXPERT));


        agent.supprimerFormateur(id);


    }


    @Test
    public void testsupprimerFormateur() {
        Formateur f = new Formateur("chiraz","demni", Poste.Ingénieur, Contrat.EXPERT,"chiraz@gmail.com","ssssssss");
        Long  id = agent.addFormateur(f);

        agent.supprimerFormateur(id);

        Optional<Formateur> op = agent.get(id);

        if (op.isPresent())
            l.error("test echec du suppression id = "+ id);

        Assert.assertFalse(op.isPresent());

    }


    @Test
    public void testnombreFormateursImpliquesDansUnCours() {


        Formateur f = new Formateur("chiraz","demni", Poste.Ingénieur, Contrat.EXPERT,"chiraz@gmail.com","ssssssss");
        Long  idf = agent.addFormateur(f);

        Cours c = new Cours("description" , TypeCours.Informatique,"DevoOps");
        Long  idc = cs.addCours(c);

        Session s =new Session(5L,"session info");
        Long ids=ss.addSession(s);

        int n = agent.nombreFormateursImpliquesDansUnCours("Informatique");
        l.warn("test nombre formateur dans le cours d info = "+n);

        ss.affecterFormateurASession(idf,ids);
        cs.affecterCoursASession(idc,ids);


        Assert.assertEquals((n + 1),agent.nombreFormateursImpliquesDansUnCours("Informatique"));



        //  suppression de formateur  =>  suppression de session => suppression de cours
        agent.supprimerFormateur(idf);
        //cs.supprimerCours(idc);
        //ss.supprimerSession(ids);

    }


    @Test
    public void testlistFormateurs() {

        int n=agent.listFormateurs().size();
        Formateur f = new Formateur("chiraz","demni", Poste.Ingénieur, Contrat.EXPERT,"chiraz@gmail.com","ssssssss");
        Long  id = agent.addFormateur(f);

        List<Formateur> list = agent.listFormateurs();
        l.info("test nouveau liste => "+ list);

        Assert.assertTrue(list.contains(agent.get(id).get()));
        Assert.assertEquals(n+1,list.size());


        agent.supprimerFormateur(id);

    }


    @Test
    public void testlistFormateursParPoste() {
        int n=agent.listFormateursParPoste(Poste.Ingénieur).size();
        Formateur f = new Formateur("chiraz","demni", Poste.Ingénieur, Contrat.EXPERT,"chiraz@gmail.com","ssssssss");
        Long  id = agent.addFormateur(f);

        List<Formateur> list = agent.listFormateursParPoste(Poste.Ingénieur);
        l.info("test nouveau liste des ingenieurs  => "+ list);


        Assert.assertTrue(list.contains(agent.get(id).get()));
        Assert.assertEquals(n+1,list.size());


        agent.supprimerFormateur(id);


    }


    @Test
    public void testnombreCoursParFormateur() {
        Formateur f = new Formateur("chiraz","demni", Poste.Ingénieur, Contrat.EXPERT,"chiraz@gmail.com","ssssssss");
        Long  idf = agent.addFormateur(f);

        Cours c = new Cours("description" , TypeCours.Informatique,"DevoOps");
        Long  idc = cs.addCours(c);

        Session s =new Session(5L,"session info");
        Long ids=ss.addSession(s);

        int n = agent.nombreCoursParFormateur(idf);
        ss.affecterFormateurASession(idf,ids);
        cs.affecterCoursASession(idc,ids);

        l.warn("test nombre initial cours = "+n +" nouveau nombre = "+agent.nombreCoursParFormateur(idf) );

        Assert.assertEquals((n + 1),agent.nombreCoursParFormateur(idf));



        //  suppression de formateur  =>  suppression de session => suppression de cours
        agent.supprimerFormateur(idf);


    }


}
