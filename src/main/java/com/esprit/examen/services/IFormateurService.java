package com.esprit.examen.services;

import java.util.List;
import java.util.Optional;

import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.Poste;
import com.esprit.examen.entities.TypeCours;

public interface IFormateurService {
	Long addFormateur(Formateur formateur);

	Long modifierFormateur(Formateur formateur);

	void supprimerFormateur(Long formateurId);
	
	int nombreFormateursImpliquesDansUnCours(String typeCours);
		
	List<Formateur> listFormateurs();

	List<Formateur> listFormateursParPoste(Poste poste);

	int nombreCoursParFormateur(Long formateurId);

	Optional<Formateur> get(Long id);
}
